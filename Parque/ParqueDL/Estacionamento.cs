﻿using ParqueBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueDL
{
    public class Estacionamento
    {
        #region ATRIBUTOS
        private Parque parqueESG;
        private Parque parqueEST;
        #endregion

        #region CONSTRUTORES
        public Estacionamento()
        {
            this.parqueESG = new Parque("ESG");
            this.parqueEST = new Parque("EST");
        }
        #endregion

        #region PROPRIEDADES
        public Parque ParqueESG
        {
            get { return this.parqueESG; }
            set { this.parqueESG = value; }
        }

        public Parque ParqueEST
        {
            get { return this.parqueEST; }
            set { this.parqueEST = value; }
        }
        #endregion

        #region METODOS
        public void AdicionaSeccao(Parque parque, int numeroLugares, bool lugaresParaReserva)
        {
            parque.Seccoes.Add(new Seccao(numeroLugares, lugaresParaReserva));
        }

        public bool EstacionaCarro(Parque parque, bool lugarReservado, Carro carroParaEstacionar)
        {
            bool resultado = false;

            foreach (var seccao in parque.Seccoes)
            {
                if (seccao.LugaresParaReserva == lugarReservado)
                {
                    if (seccao.CarrosEstacionados.Count < seccao.NumeroLugares)
                    {
                        seccao.CarrosEstacionados.Add(carroParaEstacionar);
                        resultado = true;
                    }
                }
            }

            return resultado;
        }

        public bool CarroEstacionado(Parque parque, string matricula)
        {
            bool resultado = false;

            foreach (var seccao in parque.Seccoes)
            {
                foreach (var carro in seccao.CarrosEstacionados)
                {
                    if (carro.Matricula.Equals(matricula))
                    {
                        resultado = true;
                    }
                }
            }

            return resultado;
        }
        #endregion
    }
}
