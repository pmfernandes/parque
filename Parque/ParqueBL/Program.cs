﻿using ParqueBO;
using ParqueDL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueBL
{
    class Program
    {
        private static Estacionamento estacionamentoIPCA;

        static void Main(string[] args)
        {
            // Criar o estacionamento do IPCA
            estacionamentoIPCA = new Estacionamento();

            // ParqueESG: Adicionar uma secção de 100 lugares para reservas
            estacionamentoIPCA.AdicionaSeccao(estacionamentoIPCA.ParqueESG,100, true);

            // ParqueESG: Adicionar um secção de 50 lugares
            estacionamentoIPCA.AdicionaSeccao(estacionamentoIPCA.ParqueESG,50, false);

            // ParqueEST: Adicionar uma secção de 50 lugares para reservas
            estacionamentoIPCA.AdicionaSeccao(estacionamentoIPCA.ParqueEST,50, true);

            // ParqueEST: Adcionar uma secção de 50 lugares
            estacionamentoIPCA.AdicionaSeccao(estacionamentoIPCA.ParqueEST,100, false);

            EstacionaCarro(estacionamentoIPCA.ParqueESG, false, "00-00-01", "Tinoco 1");
            EstacionaCarro(estacionamentoIPCA.ParqueESG, true, "00-00-02", "Tinoco 2");
            EstacionaCarro(estacionamentoIPCA.ParqueEST, false, "00-00-03", "Tinoco 3");
            EstacionaCarro(estacionamentoIPCA.ParqueEST, true, "00-00-04", "Tinoco 4");

            MostraResultado(estacionamentoIPCA);

            MostraCarroEstacionado(estacionamentoIPCA, "00-00-00");

            MostraCarroEstacionado(estacionamentoIPCA, "00-00-01");

            Console.ReadLine();
        }

        private static void EstacionaCarro(Parque parque, bool lugarReservado, string matricula, string nomeDono)
        {
            Carro carroParaEstacionar;

            carroParaEstacionar = new Carro("00-00-02", "Tinoco 2");
            estacionamentoIPCA.EstacionaCarro(parque, lugarReservado, carroParaEstacionar);
        }

        private static void MostraResultado(Estacionamento estacionamento)
        {
            foreach (var seccao in estacionamento.ParqueESG.Seccoes)
            {
                Console.WriteLine(estacionamento.ParqueESG.NomeParque + "\tReservados: " + seccao.LugaresParaReserva + "\tTotal: " + seccao.NumeroLugares + "\tOcupados:" + seccao.CarrosEstacionados.Count);
                MostraCarros(seccao);
                Console.WriteLine();
            }

            Console.WriteLine("---- ### ----");
            Console.WriteLine();

            foreach (var seccao in estacionamento.ParqueEST.Seccoes)
            {
                Console.WriteLine(estacionamento.ParqueEST.NomeParque + "\tReservados: " + seccao.LugaresParaReserva + "\tTotal: " + seccao.NumeroLugares + "\tOcupados:" + seccao.CarrosEstacionados.Count);
                MostraCarros(seccao);
                Console.WriteLine();
            }
            Console.WriteLine("---- ### ----");
            Console.WriteLine();
        }

        private static void MostraCarros(Seccao seccao)
        {
            foreach (var carro in seccao.CarrosEstacionados)
            {
                Console.WriteLine("Matricula: " + carro.Matricula +"\tNome Dono: " + carro.NomeDono);
            }
        }

        private static void MostraCarroEstacionado(Estacionamento estacionamento, string matriculaCarroEstacionado)
        {
            bool carroEstacionadoNoIPCA;

            carroEstacionadoNoIPCA = estacionamento.CarroEstacionado(estacionamento.ParqueESG, matriculaCarroEstacionado) | estacionamento.CarroEstacionado(estacionamento.ParqueEST, matriculaCarroEstacionado);
            Console.WriteLine("Matricula: " + matriculaCarroEstacionado + "\tCarro Estacionado: " + carroEstacionadoNoIPCA);
        }
    }
}
