﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueBO
{
    public class Seccao
    {
        #region ATRIBUTOS

        private int numeroLugares;
        private bool lugaresParaReserva;
        private List<Carro> carrosEstacionados;

        #endregion

        #region CONSTRUTORES

        public Seccao()
        {
            this.carrosEstacionados = new List<Carro>();
        }

        public Seccao(int numeroLugares, bool lugaresParaReserva)
        {
            this.numeroLugares = numeroLugares;
            this.lugaresParaReserva = lugaresParaReserva;
            this.carrosEstacionados = new List<Carro>();
        }
        #endregion

        #region PROPRIEDADES
        public int NumeroLugares
        {
            get { return this.numeroLugares; }
            set { this.numeroLugares = value; }
        }

        public bool LugaresParaReserva
        {
            get { return this.lugaresParaReserva; }
            set { this.lugaresParaReserva = value; }
        }

        public List<Carro> CarrosEstacionados
        {
            get { return this.carrosEstacionados; }
            set { this.carrosEstacionados = value; }
        }
        #endregion
    }
}
