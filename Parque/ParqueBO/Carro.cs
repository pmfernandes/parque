﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueBO
{
    public class Carro
    {
        #region ATRIBUTOS

        private string matricula;
        private string nomeDono;

        #endregion

        #region CONSTRUTORES

        public Carro()
        {
        }

        public Carro(string matricula, string nomeDono)
        {
            this.matricula = matricula;
            this.nomeDono = nomeDono;
        }
        #endregion

        #region PROPRIEDADES

        public string Matricula
        {
            get { return this.matricula; }
            set { this.matricula = value; }
        }

        public string NomeDono
        {
            get { return this.nomeDono; }
            set { this.nomeDono = value; }
        }

        #endregion
    }
}
