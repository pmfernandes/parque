﻿using ParqueBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParqueBO
{
    public class Parque
    {
        #region ATRIBUTOS
        private string nomeParque;
        private List<Seccao> seccoes;
        #endregion

        #region CONSTRUTORES
        public Parque(string nomeParque)
        {
            this.nomeParque = nomeParque;
            this.seccoes = new List<Seccao>();
        }
        #endregion

        #region PROPRIEDADES
        public string NomeParque
        {
            get { return this.nomeParque; }
            set { this.nomeParque = value; }
        }

        public List<Seccao> Seccoes
        {
            get { return this.seccoes; }
            set { this.seccoes = value; }
        }
        #endregion
    }
}
